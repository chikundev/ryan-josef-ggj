-- chikun :: 2015
-- Crucifix item


-- Temporary item, removed at end of script
local crucifix = {
    name    = "Crucifix",
    info    = "An adorned cross from the statue of Jesus.",     -- Inventory description
    textCol = { 227, 243, 51 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function crucifix:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if object == "circle" then

        launchText({
                { "Senemut", "That which changes the soul."},
                { "", "The circle has accepted the crucifix."}
            })

        item.setUsed('crucifix')

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return crucifix
