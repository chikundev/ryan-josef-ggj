-- chikun :: 2015
-- Cocaine item


-- Temporary item, removed at end of script
local cocaine = {
    name    = "Cocaine",
    info    = "Premium yayo from Pee-Wee Herman.",     -- Inventory description
    textCol = { 240, 30, 150 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function cocaine:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if object == "circle" then

        launchText({
                { "Senemut", "That which changes the mind."},
                { "", "The circle has accepted the cocaine."}
            })

        item.setUsed('cocaine')

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return cocaine
