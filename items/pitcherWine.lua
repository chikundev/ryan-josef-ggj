-- chikun :: 2015
-- Wine Pitcher item


-- Temporary item, removed at end of script
local pitcherWine = {
    name    = "Wine Pitcher",
    info    = "A pitcher filled with wine.",     -- Inventory description
    textCol = { 200, 10, 40 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function pitcherWine:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if object == "chalice1" then

        fillChalice(1)
    elseif object == "chalice2" then

        fillChalice(2)
    elseif object == "chalice3" then

        fillChalice(3)
    elseif object == "chalice4" then

        fillChalice(4)
    elseif object == "circle" and not wineUsed then

        launchText({
                { "Senemut", "That which changes the body."},
                { "", "The circle has accepted a drop of wine."}
            })

        wineUsed = true

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return pitcherWine
