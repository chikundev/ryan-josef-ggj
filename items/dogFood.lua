-- chikun :: 2015
-- Dog food item


-- Temporary item, removed at end of script
local dogFood = {
    name    = "Dog Food",
    info    = "High in calcium.",     -- Inventory description
    textCol = { 140, 105, 240 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function dogFood:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if object == "cosmicDog" then

        -- Perform actions
        obj[map.current.name]["cosmicDog"]:onTouch()

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return dogFood
