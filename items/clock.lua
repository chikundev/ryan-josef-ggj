-- chikun :: 2015
-- Clock item


-- Temporary item, removed at end of script
local clock = {
    name    = "Clock",
    info    = "Yeah, this is a clock.",     -- Inventory description
    textCol = { 155, 255, 200 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function clock:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if map.current.name == "test" and object == "doorBack" then

        launchText({
                { "", "It really had no visible effect." },
                { "", "This sucks, man." }
            })

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return clock
