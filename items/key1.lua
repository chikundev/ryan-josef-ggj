-- chikun :: 2015
-- Key 1 item


-- Temporary item, removed at end of script
local key1 = {
    name    = "Gold Key",
    info    = "Recieved from Cosmic Dog.",     -- Inventory description
    textCol = { 255, 215, 0 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function key1:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if object == "anything" then

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return key1
