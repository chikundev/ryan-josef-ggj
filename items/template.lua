-- chikun :: 2015
-- Template for all items


-- Temporary item, removed at end of script
local newItem = {
    name    = "",
    info    = "",           -- Inventory description
    textCol = { 255, 255, 255 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function newItem:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if object == "anything" then

        -- Perform actions

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return newItem
