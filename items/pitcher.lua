-- chikun :: 2015
-- Pitcher item


-- Temporary item, removed at end of script
local pitcher = {
    name    = "Pitcher",
    info    = "An empty pitcher.",     -- Inventory description
    textCol = { 10, 140, 200 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function pitcher:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if object == "winepool" then

        launchText({
                { "", "You fill the pitcher with wine." }
            })

        item.modify('pitcher', 'pitcherWine')

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return pitcher
