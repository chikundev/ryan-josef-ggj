-- chikun :: 2015
-- Template for all items


-- Temporary item, removed at end of script
local unholyLiquid = {
    name    = "Unholy Liquid",
    info    = "For Danny Elfman.",           -- Inventory description
    textCol = { 250, 5, 5 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function unholyLiquid:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if object == "dannyFrame" or object == "dannyBlank" then

        triggerDialogue("dannyAfterBowl")

        -- Perform actions

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return unholyLiquid
