-- chikun :: 2015
-- Rubber Duck item


-- Temporary item, removed at end of script
local duck = {
    name    = "Rubber Duck",
    info    = "Not a rubber duck.",     -- Inventory description
    textCol = { 255, 255, 51 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function duck:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if object == "anything" then

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return duck
