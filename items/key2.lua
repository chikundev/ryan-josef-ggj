-- chikun :: 2015
-- Key 2 item


-- Temporary item, removed at end of script
local key2 = {
    name    = "Silver Key",
    info    = "Recieved from The Fire Room.",     -- Inventory description
    textCol = { 192, 192, 192 } -- Colour of name in inventory
}


-- Performed when an item is used with an object
function key2:onUse(object)

    -- Item that this object was used with
    local usedWith = obj[map.current.name][object]

    -- Check objects
    if object == "anything" then

    -- If none of assigned objects...
    else

        -- ...report failure
        return false

    end

    -- Otherwise report success
    return true

end


-- Transfer data to item loading script
return key2
