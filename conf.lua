-- chikun :: 2015
-- The configuration file for our tutorial


--[[
    The function which runs at the start of the game, and
    allows us to modify the game window
  ]]

require "src/flags"

function love.conf(game)

    if isOUYA then
        game.window.width   = 256 * 4
        game.window.height  = 144 * 4
    else
        game.window.width   = 256 * 3
        game.window.height  = 192 * 3
    end
    game.window.title   = "Samsara Interactive"
    game.window.resizable = true
    game.window.fullscreen = false

    --[[
        On Windows, will attach a debug console to the game.
        Set to false before distribution.
      ]]
    game.console = false

end
