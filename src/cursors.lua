cursor = { }



cursor.examine = {
    offsetX = 2,
    offsetY = 2,
    image   = gfx.cursor.examine
}

cursor.interact = {
    offsetX = 7,
    offsetY = 0,
    image   = gfx.cursor.interact
}


cursor.pointLeft = {
    offsetX = 0,
    offsetY = 8,
    image   = gfx.cursor.pointLeft
}


cursor.pointRight = {
    offsetX = 15,
    offsetY = 8,
    image   = gfx.cursor.pointRight
}


cursor.use = {
    offsetX = 2,
    offsetY = 2,
    image   = gfx.cursor.use
}


cursor.wait = {
    offsetX = 8,
    offsetY = 8,
    image   = gfx.cursor.wait
}
