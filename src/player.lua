-- chikun :: 2015
-- Player script



player = {}



-- Called when map loads
player.set = function(x, y, w, h)


    -- Set player position
    player.x = x ; player.y = y
    player.w = w ; player.h = h

    player.facing = 1

    player.imageTable = {
        gfx.player,
        gfx.player2
    }

    player.imageStep = 1

    -- Set previous positioning
    player.xPrevious = player.x
    player.yPrevious = player.y

    -- Set player image
    player.image = gfx.player

    -- For player movement
    player.gravity      = 2048
    player.ySpeed       = 0
    player.ySpeedMax    = 768

    -- Set player score
    player.score = 0


end



-- Called using update phase
player.update = function(dt)

    player.imageStep = player.imageStep + dt

    if player.imageStep >= (#player.imageTable + 1) then

        player.imageStep = player.imageStep - #player.imageTable

    end

    if not isOUYA then
        -- Does player have a place to go?
        if (player.destination) then


            local standX, standY =
                player.x + 6,
                player.y + 18

            -- Determine position to move to
            local posXTo, posYTo =
                player.destination.x,
                player.destination.y

            newFlag = false

            local pathClear = true
            local slopeX, slopeY =
                posXTo - standX,
                posYTo - standY

            for i = 0, 4 do

                if math.overlapTable(collisions, {
                            x = standX + slopeX * (i / 16),
                            y = standY + slopeY * (i / 16),
                            w = 1,
                            h = 1
                        }) then
                    pathClear = false
                end

            end

            -- Determine angle to move along
            local angle = math.atan2((posYTo - player.y),
                        (posXTo - player.x))


            --if pathClear then


                -- Actually move player
                player.x = player.x + math.cos(angle) * dt * 96

                while math.overlapTable(collisions, player) do
                    player.x = math.round(player.x - math.sign(math.cos(angle)))
                end

                player.y = player.y + math.sin(angle) * dt * 96

                while math.overlapTable(collisions, player) do
                    player.y = math.round(player.y - math.sign(math.sin(angle)))
                end


                -- If player is close to destination or at same position as last time...
                if (math.dist(player.x, player.y, posXTo, posYTo) < 2) then

                    -- Set player's position to destination
                    player.x = posXTo ; player.y = posYTo

                    -- If a click...
                    if player.destination.type == "click" then

                        -- Run the onTouch script for the object
                        obj[map.current.name][player.destination.object]:onTouch()

                    end

                    -- Nullify destination
                    player.destination = nil

                end
        end
    else

    end


    -- Constrain player movement to a rectangle
    player.x = math.clamp(32,
                    player.x,
                    map.current.w * map.current.tileW - 32 - player.w)
    player.y = math.clamp(128 - player.h,
                    player.y,
                    144 - player.h)

    if math.sign(player.x - player.xPrevious) ~= 0 then
        player.facing = math.sign(player.x - player.xPrevious)
    end

    -- Update previous position
    player.xPrevious = player.x
    player.yPrevious = player.y


end



-- Called using draw phase
player.draw = function()

    local scale = 1 - (math.abs(player.y - 144)) / 288

    g.draw(player.imageTable[math.floor(player.imageStep)], player.x + player.w / 2,
        player.y + player.h * 0.75, 0, scale * player.facing, scale,
    player.w / 2, player.h * 0.75)

end
