-- chikun :: 2015
-- Map state


-- Temporary state, removed at end of script
local mapState = { }


-- On state create
function mapState:create()

    fade = 0

    fadeDir = 1

    backButton = {
            name = "back",
            text = "Return",
            x = 172,
            y = 164,
            w = 80,
            h = 24
        }

end


-- On state update
function mapState:update(dt)

    bottomText = ""

    hoverText = ""

    fade = math.clamp(0, fade + fadeDir * dt, 1)

    if fade == 1 then

        for key, button in ipairs(roomButtons) do

            if math.overlap(guiCursor, button) then

                hoverText = roomNames[button.name] or "Unnamed Room"

                if roomCompleted[button.name] then

                    bottomText = "Complete"

                else

                    bottomText = "Incomplete"

                end

                if button.name == map.current.name then

                    bottomText = bottomText .. "\nCurrent room"

                end

            end
        end

        if clicking and not m.isDown('l') then

            if math.overlap(guiCursor, backButton) then

                fadeDir = -1

            end
        end

    elseif fade == 0 then

        state.set(states.play)

    end

    clicking = m.isDown('l')

end


-- On state draw
function mapState:draw()

    g.setColor(255, 255, 255)

    gui.draw()

    if fade < 0.5 then

        states.play:draw()

    else

        g.setColor(0, 0, 0)

        g.rectangle('fill', 0, 0, 256, 192)

        g.setFont(fnt.small)

        g.setColor(255, 255, 255)

        g.print(hoverText, 2, 2)

        local tool = backButton

        g.draw(gfx.cursor.pointLeft, tool.x + 2, tool.y + 4)

        g.print(tool.text, tool.x + 20, tool.y + 6)

        g.rectangle('line', tool.x, tool.y, tool.w, tool.h)

        g.print(bottomText or "", 2, 162)

        g.draw(gfx.mapbackground, 0, 16)

        for key, button in ipairs(roomButtons) do

            if button.name == map.current.name then

                g.setColor(255, 255, 255)

                g.rectangle('fill', button.x - 1, button.y - 1,
                    button.w + 2, button.h + 2)

            end

            g.setColor(200, 40, 40)

            if roomCompleted[button.name] then

                g.setColor(100, 200, 40)

            end

            g.rectangle('fill', button.x, button.y,
                button.w, button.h)

        end

    end

    g.setColor(0, 0, 0, 255 - (math.abs((fade * 2) - 1) * 255))

    g.rectangle('fill', 0, 0, 256, 192)

end


-- On state kill
function mapState:kill()

end



function completeRoom()

    state.load(states.complete)

    roomCompleted[map.current.name] = true

end



-- Transfer data to state loading script
return mapState
