-- chikun :: 2014
-- Splash state


-- Temporary state, removed at end of script
local splashState = { }


-- On state create
function splashState:create()


    -- Set background colour to black
    g.setBackgroundColor(0, 0, 0)

    -- Set off timer
    timer = 0

    -- Has sound played
    soundPlayed = false


end


-- On state update
function splashState:update(dt)


    -- Increase timer
    timer = timer + dt

    -- When one second passes, change state...
    if (timer > 1) then

        -- ...to the play state
        state.change(states.title)

    -- If over half a second and sound hasn't played...
    elseif (timer >= 0.5 and not soundPlayed) then

        -- ...then play it!
        sfx.startup:play()

        -- And set variable
        soundPlayed = true

    end


end


-- On state draw
function splashState:draw()


    -- Local alpha
    local alpha = (1 - math.abs(timer - 0.5) * 2) * 256


    -- Set colour with alpha
    g.setColor(255, 255, 255, math.max(alpha - 1, 0))

    -- Set font
    g.setFont(fnt.splash)

    -- Draw logo with formatting
    local drop = 96
    if isOUYA then
        drop = 72
    end
    g.printf("chikun", 128, drop - (fnt.splash:getHeight() / 2), 0, 'center')


end


-- On state kill
function splashState:kill()


    -- Kill all variables
    timer = nil
    soundPlayed = nil


end


-- Transfer data to state loading script
return splashState
