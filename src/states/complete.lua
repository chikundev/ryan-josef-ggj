-- chikun :: 2014
-- Complete state


-- Temporary state, removed at end of script
local completeState = { }


-- On state create
function completeState:create()

    sfx.startup:play()

    t.sleep(0.75)

    timer = 0

end


-- On state update
function completeState:update(dt)

    timer = timer + dt

    if timer >= 1 then

        state.set(states.play)

    end

end


-- On state draw
function completeState:draw()

    states.play:draw()

    g.setColor(255, 255, 255, math.abs(math.sin((timer * 360) * math.pi / 180)) * 255)

    g.rectangle('fill', 0, 16, 256, 144)

end


-- On state kill
function completeState:kill()

end


-- Transfer data to state loading script
return completeState
