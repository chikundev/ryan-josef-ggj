-- chikun :: 2014
-- Play state


-- Temporary state, removed at end of script
local playState = { }


-- On play state create
function playState:create()


    oldBGM = bgm.loop

    -- Create the GUI
    gui.create()


    -- Set our map to the test map
    map.swap(maps.startRoom)
    --map.swap(maps.room3, door2Right)


    -- Create our view object
    view = {
        x = 0,
        y = 0,
        w = 256,
        h = 144
    }


    -- Create our gameScreen object
    gameScreen = {
        x = 0,
        y = 16,
        w = 256,
        h = 144
    }


end


-- On play state update
function playState:update(dt)


    if not roomCompleted[map.current.name] and
        winChecks[map.current.name] then

        if winChecks[map.current.name]() then

            completeRoom()

        end

    end


    -- Empty hover text
    hoverText = roomNames[map.current.name] or ""


    -- Update player
    player.update(dt)


    -- Run current tool
    tools[currentTool](dt)


    -- Update view to follow player, but restrict it based on map data
    view.x = math.clamp(0, player.x + player.w / 2 - view.w / 2,
        map.current.w * map.current.tileW - view.w)
    view.y = math.clamp(0, player.y + player.h / 2 - view.h / 2,
        map.current.h * map.current.tileH - view.h)


    if ritualComplete and not ritualSpoke then

        launchText({
                { "Senemut", "The ritual is complete." },
                { "", "Senemut hands you a bowl of Unholy Liquid." }
            })

        item.collect('unholyLiquid')

        ritualSpoke = true

    end

end


-- On play state draw
function playState:draw()


    -- Move drawing position of objects based on view position
    if isOUYA then

        g.translate(-view.x, -view.y)

    else

        g.translate(-view.x, -view.y + 16)

    end

    -- Draw the map
    map.draw()

    g.origin()

    if isOUYA then

        g.scale(g.getWidth() / 256, g.getHeight() / 144)

        g.translate(-view.x, -view.y)

        g.setColor(96, 128, 255)

        for key, value in ipairs(objects) do

            local offset = {obj[map.current.name][value.name]:getOffset()}

            g.rectangle('fill', value.x + offset[1] - 8,
                value.y + offset[2] - 3, 16, 6)

        end

    else

        g.scale(g.getWidth() / 256, g.getHeight() / 192)

        g.translate(-view.x, -view.y + 16)

    end

    g.setColor(255, 255, 255)

    -- Draw player at its position
    player.draw()

    -- Reset the drawing position
    g.origin()

    -- Scale this shit UP
    if isOUYA then
        g.scale(g.getWidth() / 256, g.getHeight() / 144)
    else
        g.scale(g.getWidth() / 256, g.getHeight() / 192)
    end

    -- Draw GUI
    gui.draw()


end


-- On play state kill
function playState:kill()


    -- Kill variables
    coins       = nil
    collisions  = nil


    -- Stop the background music
    bgm.loop:stop()


end


-- Transfer data to state loading script
return playState
