-- chikun :: 2014
-- Examine state


-- Temporary state, removed at end of script
local examineState = { }


-- On state create
function examineState:create()

    examineText = examineObject or { { "", "That's not very interesting." } }

    examineStep = 1

    examineShow = 0

end


-- On state update
function examineState:update(dt)

    examineShow = math.min(examineText[examineStep][2]:len(), examineShow + 100 * dt)

    hoverText = examineText[examineStep][1]

    if clicking and not m.isDown('l') then

        sfx.click:play()

        if examineShow == examineText[examineStep][2]:len() then

            examineStep = examineStep + 1

            examineShow = 0

            if examineStep > #examineText then

                state.set(states.play)

            end

        else

            examineShow = examineText[examineStep][2]:len()

        end
    end

    clicking = m.isDown('l')

end


-- On state draw
function examineState:draw()

    local text = examineText[examineStep][2]

    -- Draw play state
    states.play:draw()

    -- Draw bottom bar
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 160, 256, 32)

    -- Draw examine text
    g.setFont(fnt.small)
    g.setColor(255, 255, 255)
    g.printf(text:sub(1, math.ceil(examineShow)), 2, 162, 252, 'left')

end


-- On state kill
function examineState:kill()

    examineObject = nil

    examineText   = nil

end


function launchText(dialog)

    examineObject = dialog

    state.load(states.examine)

end


-- Transfer data to state loading script
return examineState
