-- chikun :: 2014
-- Menu state


-- Temporary state, removed at end of script
local menuState = { }


-- On state create
function menuState:create()

    -- On creation, click and reset cursor
    sfx.click:play()
    hoverText = "Menu"
    cursorImage = "interact"

    -- Set background colour to a purple
    g.setBackgroundColor(96, 32, 64)


    -- Play the background music
    -- bgm.loop:play()

    menuButtons = {
        {
            text = "Resume",
            x = 8,
            y = 104,
            w = 64,
            h = 24
        },
        {
            text = "Options",
            x = 8,
            y = 72,
            w = 64,
            h = 24
        }--[[,
        {
            text = "Quit",
            x = 8,
            y = 124,
            w = 64,
            h = 24
        }]]
    }

    menuSelected = 2


end


-- On state update
function menuState:update(dt)


    if not clicking and m.isDown('l') then

        noteVol = a.getVolume()
        noteSca = global.scale

        for key, button in ipairs(menuButtons) do

            if math.overlap(button, guiCursor) then

                -- Set selected
                menuSelected = key

                -- If resume...
                if key == 1 then

                    -- ...resume
                    state.set(states.play)

                    sfx.click:play()

                end
            end
        end
    end

    if m.isDown('l') and menuSelected == 2 then

        if math.overlap(guiCursor, {x = 128, y = 78, w = 120, h = 12}) then

            a.setVolume(math.clamp(0, (guiCursor.x - 133) / 110, 1))
        elseif math.overlap(guiCursor, {x = 128, y = 110, w = 120, h = 12}) and
            s.getOS() ~= "Android" then

            noteSca = math.clamp(0, math.round((guiCursor.x - 133) / 110 * 3), 3) + 1
        end
    end

    if clicking and not m.isDown('l') then

        if noteVol ~= a.getVolume() then

            sfx.click:play()

        elseif noteSca ~= global.scale then

            rescaleTo(noteSca)

            noteSca = nil

        end
    end

    -- Set clicking value
    clicking = m.isDown('l')

end


-- On state draw
function menuState:draw()


    -- Draw play state
    states.play:draw()

    -- Draw background
    g.setColor(0, 0, 0, 160)
    g.rectangle('fill', 0, 16, 256, 176)
    g.rectangle('fill', 224, 0, 32, 16)

    -- Set font
    g.setColor(255, 255, 255)
    g.setFont(fnt.regular)


    g.printf("Game Paused", 0, 16, 256, 'center')

    g.setFont(fnt.small)


    for key, button in ipairs(menuButtons) do

        if key ~= menuSelected then

            g.rectangle('line', button.x, button.y, button.w, button.h)

            g.printf(button.text, button.x, button.y + 6, button.w, 'center')

        else

            g.printf(button.text .. " >", button.x, button.y + 6, button.w, 'right')

        end

    end


    if menuSelected == 2 then

        -- Audio tag
        g.print("Audio", 80, 78)

        -- Audio bar
        g.rectangle('line', 128, 78, 120, 12)

        -- Audio slider
        g.rectangle('line', 128 + (110 * a.getVolume()), 78, 10, 12)

        if s.getOS() == "Android" then

            -- Scale tag
            g.print("Scaling option TBA", 80, 110)

        else

            -- Scale tag
            g.print("Scale", 80, 110)

            -- Scale bar
            g.rectangle('line', 128, 110, 120, 12)

            -- Scale slider
            g.rectangle('line', 128 + (((noteSca or global.scale) - 1) * 110 / 3),
                110, 10, 12)

        end

    else



    end


end


-- On state kill
function menuState:kill()

end


-- Transfer data to state loading script
return menuState
