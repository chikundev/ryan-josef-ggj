-- chikun :: 2014
-- Faceless panic state


-- Temporary state, removed at end of script
local faceState = { }


-- On state create
function faceState:create()

    timer = 0

    cursorImage = "interact"

    mask = {
        x = 13.6 * 16,
        y = 40
    }

    viewX = 256

    alpha = 0

    sfx.scary:stop()

end


-- On state update
function faceState:update(dt)

    timer = timer + dt

    if timer < 2 and timer >= 1 then

        viewX = -((math.clamp(1, timer, 2) - 1) * 152 - 256)

    elseif timer < 3 and timer >= 2 then

        if not sfx.scary:isPlaying() then

            sfx.scary:setVolume(0.75)

            sfx.scary:play()

        end

        mask.y = 40 + (timer - 2) * 40

    elseif timer < 4 and timer >= 3 then



    elseif timer < 5 and timer >= 4 then

        mask.x = 13.6 * 16 + 500 * (timer - 4)

        viewX = viewX + (dt * 152)

        alpha = math.min((timer - 4) * 2, 1)

    elseif timer < 6 and timer >= 5 then

        if map.current.name ~= "room4" then

            map.swap(maps.room4, "door2Left")

            states.play:update(dt)

        end

        alpha = 6 - timer

    elseif timer >= 6 then

        state.set(states.play)

        spiritsAngry = nil

        spiritHappy1 = nil
        spiritHappy2 = nil
        spiritHappy3 = nil

    end
end


-- On state draw
function faceState:draw()

    g.setColor(255, 255, 255)

    states.play:draw()

    g.setColor(255, 255, 255, math.min(timer, 1) * 255)

    g.translate(-viewX, 16)

    if timer < 5 then

        map.draw(maps.room3scary)

        g.draw(gfx.mask5, mask.x, mask.y)

        player.draw()

    end

    g.origin()

    g.scale(g.getWidth() / 256, g.getHeight() / 192)

    g.setColor(255, 255, 255, alpha * 255)

    g.rectangle('fill', 0, 16, 256, 144)

end


-- On state kill
function faceState:kill()

end


-- Transfer data to state loading script
return faceState
