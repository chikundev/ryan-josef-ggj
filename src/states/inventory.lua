-- chikun :: 2014
-- Inventory state


-- Temporary state, removed at end of script
local invState = { }


-- On state create
function invState:create()

    itemBlocks = { }

    for y = 0, 6 do
        for x = 0, 3 do
            itemBlocks[#itemBlocks + 1] = {
                x = 115 + x * 34,
                y = 26 + y * 18,
                w = 32,
                h = 16
            }
        end
    end

    hoverItem = nil

    cursorImage = "interact"

    backButton = {
            name = "back",
            text = "Return",
            x = 172,
            y = 164,
            w = 80,
            h = 24
        }

end


-- On state update
function invState:update(dt)

    hoverText = "Inventory"

    hoverItem = nil

    for key, block in ipairs(itemBlocks) do

        if math.overlap(block, guiCursor) and #itemList >= key then

            if (itemList[key].held or itemList[key].used) then

                hoverItem = itemList[key]

                hoverItemKey = key

            end
        end
    end

    if m.isDown('l') and not clicking then

        if hoverItem then

            clickBox = hoverItem.name

        elseif math.overlap(backButton, guiCursor) then

            clickBox = "back"

        end
    end

    if not m.isDown('l') and clicking then

        if hoverItem then

            if clickBox == hoverItem.name and itemList[hoverItemKey].held and not itemList[hoverItemKey].used then

                useObject = items[hoverItem.name]

                state.set(states.play)

                sfx.click:play()

                currentTool = "use"

            end
        end

        if math.overlap(backButton, guiCursor) and clickBox == "back" then

            -- ...resume
            state.set(states.play)

            sfx.click:play()

            currentTool = "interact"

        end
    end

    clicking = m.isDown('l')

end


-- On state draw
function invState:draw()

    -- Draw play state
    states.play:draw()

    -- Draw background
    g.setColor(0, 0, 0, 160)
    g.rectangle('fill', 0, 16, 256, 176)
    g.rectangle('fill', 224, 0, 32, 16)

    -- Draw bottom bar
    g.setColor(0, 0, 0)
    g.rectangle('fill', 0, 160, 256, 32)

    -- Blocks
    g.setColor(192, 192, 192)
    g.rectangle('fill', 8, 26, 105, 101)
    for key, box in ipairs(itemBlocks) do
        g.rectangle('fill', box.x, box.y, box.w, box.h)
    end

    -- Items
    for key, object in ipairs(itemList) do
        if object.held then

            g.setColor(255, 255, 255)

        elseif object.used then

            g.setColor(128, 0, 0)

        else

            g.setColor(0, 0, 0)

        end

        g.draw(gfx.items[object.name], itemBlocks[key].x + 8, itemBlocks[key].y, 0, 0.5)

        if object.used then

            g.setColor(255, 255, 255)

            g.draw(gfx.items.used, itemBlocks[key].x + 8,
                itemBlocks[key].y, 0, 0.5)

        end

    end

    -- If hovering over something
    if hoverItem then

        g.setColor(255, 255, 255)

        g.draw(gfx.items[hoverItem.name], 8 + 52 - 16, 30 + 50 - 16)

        local text = items[hoverItem.name].name

        if hoverItem.used then

            text = text .. " (Used)"

        end

        g.setColor(items[hoverItem.name].textCol or { 255, 255, 255 })
        g.setFont(fnt.small)
        g.print(text, 2, 146)
        g.setColor(0, 0, 0)
        g.setFont(fnt.smallre)
        g.print(text, 2, 146)

        g.setColor(255, 255, 255)
        g.setFont(fnt.small)
        g.printf(items[hoverItem.name].info, 2, 162, 168, 'left')

    end

    g.setColor(255, 255, 255)

    g.setFont(fnt.small)

    local tool = backButton

    g.draw(gfx.cursor.pointLeft, tool.x + 2, tool.y + 4)

    g.print(tool.text, tool.x + 20, tool.y + 6)

    g.rectangle('line', tool.x, tool.y, tool.w, tool.h)

end


-- On state kill
function invState:kill()

end


-- Transfer data to state loading script
return invState
