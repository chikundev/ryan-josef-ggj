-- chikun :: 2014
-- Fade between maps state


-- Temporary state, removed at end of script
local fadeState = { }


-- On state create
function fadeState:create()

    timer       = 0
    mapChanged  = false

end


-- On state update
function fadeState:update(dt)

    -- Increase timer
    timer = timer + dt

    if timer >= 0.5 and not mapChanged then

        map.swap(global.nextMap, global.spawnAt)

        states.play:update(dt)

        mapChanged = true

    end


    -- If timer larger than 1
    if timer >= 1 then

        -- Go back to gameplay
        state.set(states.play)

    end

    -- Clear hoverText
    hoverText   = ""

    -- Show waiting cursor
    cursorImage = "wait"

end


-- On state draw
function fadeState:draw()

    states.play:draw()

    g.setColor(0, 0, 0, 255 - 255 * math.abs(0.5 - timer) * 2)

    if not isOUYA then
        g.rectangle("fill", 0, 16, 256, 144)
    else
        g.rectangle('fill', 0, 0, 256, 144)
    end

end


-- On state kill
function fadeState:kill()

end


-- Transfer data to state loading script
return fadeState
