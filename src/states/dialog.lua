-- chikun :: 2014
-- Dialog state


-- Temporary state, removed at end of script
local dialogState = { }


-- On state create
function dialogState:create()

    KKK = nil

    diaFade = 0

    diaFadeDir = 1

    diaStep = 1
    diaShow = 0

    bgImage = diaFile[diaStep].bgImage

    g.setBackgroundColor(0, 0, 0)

    local offs = 0
    if isOUYA then
        offs = 16
    end

    choiceButtons = {
        {
            x = 24,
            y = 68 - offs,
            w = 80,
            h = 24
        },
        {
            x = 152,
            y = 68 - offs,
            w = 80,
            h = 24
        }
    }

end


-- On state update
function dialogState:update(dt)

    if diaFade < 1 and diaFadeDir == 1 then

        diaFade = math.min(diaFade + dt, 1)

    elseif diaFade > 0 and diaFadeDir == -1 then

        diaFade = math.max(diaFade - dt, 0)

    elseif diaFade == 0 and diaFadeDir == -1 then

        state.set(states.play)

    else

        if diaFile[diaStep].code and not KKK then

            diaFile[diaStep].code()

            KKK = true

        end

        diaShow = math.min(diaFile[diaStep].text:len(), diaShow + 100 * dt)

        hoverText = ""

        if clicking and not m.isDown('l') then

            sfx.click:play()

            if diaShow == diaFile[diaStep].text:len() then

                if not diaFile[diaStep].choices then

                    diaStep = diaFile[diaStep].next or (diaStep + 1)

                    KKK = nil

                    if diaStep > #diaFile or
                        diaFile[diaStep - 1].finish then

                        diaStep = diaStep - 1

                        diaFadeDir = -1

                    else

                        bgImage = diaFile[diaStep].bgImage or bgImage

                        diaShow = 0

                    end

                else

                    for key, button in ipairs(choiceButtons) do

                        if math.overlap(button, guiCursor) then

                            diaFile[diaStep].choices[key][2]()

                        end

                    end

                end

            else

                diaShow = diaFile[diaStep].text:len()

            end
        end
    end

    clicking = m.isDown('l')

end


-- On state draw
function dialogState:draw()

    g.setBackgroundColor(0, 0, 0)

    if diaFade < 1 then

        g.setColor(255, 255, 255)

        states.play:draw()

        if not isOUYA then
            g.setColor(0, 0, 0, 255 * diaFade)

            g.rectangle('fill', 0, 0, 256, 16)

            g.rectangle('fill', 0, 160, 256, 32)
        end

    end

    local text = diaFile[diaStep].text

    g.setColor(255, 255, 255, diaFade * 255)

    if isOUYA then
        g.draw(bgImage, 0, 0)
        g.setColor(0, 0, 0, 192 * diaFade)
        g.rectangle('fill', 0, 112, 256, 32)
        g.setColor(255, 255, 255, diaFade * 255)
    else
        g.draw(bgImage, 0, 16)
    end

    g.setFont(fnt.small)

    if diaShow == diaFile[diaStep].text:len() and diaFile[diaStep].choices then

        for key, button in ipairs(choiceButtons) do

            g.setColor(0, 0, 0, diaFade * 255)

            g.rectangle('fill', button.x, button.y,
                button.w, button.h)

            g.setColor(255, 255, 255, diaFade * 255)

            g.rectangle('line', button.x, button.y,
                button.w, button.h)

            g.printf(diaFile[diaStep].choices[key][1],
                button.x, button.y + 6, button.w, 'center')

        end

    end

    local offs = 0
    if isOUYA then
        offs = 48
    end

    g.printf(text:sub(1, math.ceil(diaShow)), 2, 162 - offs, 252, 'left')
    g.setFont(fnt.smallre)
    g.setColor(0, 0, 0, diaFade * 255)
    g.printf(text:sub(1, math.ceil(diaShow)), 2, 162 - offs, 252, 'left')
    g.setFont(fnt.small)
    g.setColor(255, 255, 255, diaFade * 255)

    g.setColor(speakers[diaFile[diaStep].speaker].col[1],
        speakers[diaFile[diaStep].speaker].col[2],
        speakers[diaFile[diaStep].speaker].col[3],
        diaFade * 255)
    g.setFont(fnt.small)
    g.print(speakers[diaFile[diaStep].speaker].name, 2, 146 - offs)
    g.setColor(0, 0, 0, diaFade * 255)
    g.setFont(fnt.smallre)
    g.print(speakers[diaFile[diaStep].speaker].name, 2, 146 - offs)

end


function dialogState:kill()

end


-- Transfer data to state loading script
return dialogState
