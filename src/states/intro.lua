-- chikun :: 2014
-- intro state


-- Temporary state, removed at end of script
local introState = { }


-- On state create
function introState:create()

    introText = {
        { "", "The year is 2XXX." },
        { "", "The universe meets its final cosmic cycle." },
        { "", "The Earth transforms, a place unreal and incorporeal." },
        { "", "Time, space and form converge as the cycle ends:" },
        { "", "The cosmic singluarity ready to reforge the fabric of reality anew." },
        { "", "In doing so, everything will be destroyed."},
        { "", "You have infiltrated the Cycle." },
        { "", "You must preserve it at all costs." },
    }

    introStep = 1

    introShow = 0

end


-- On state update
function introState:update(dt)

    if fade then

        hoverText = ""

        fade = fade + dt

        if fade >= 1 then
            state.current = states.play
            bgm.title:stop()
        end

    else

        introShow = math.min(introText[introStep][2]:len(),
                introShow + 100 * dt)

        hoverText = introText[introStep][1]

        if clicking and not m.isDown('l') then

            sfx.click:play()

            if introShow == introText[introStep][2]:len() then

                introStep = introStep + 1

                introShow = 0

                if introStep > #introText then

                    fade = 0

                    introStep = introStep - 1
                    introShow = introText[introStep][2]:len()

                    states.play:create()
                    clicking = false
                    -- Cursor position relevant to game screen
                    gameCursor = {
                        x = guiCursor.x,
                        y = guiCursor.y - 16,
                        w = 1,
                        h = 1
                    }
                    states.play:update(dt)

                end

            else

                introShow = introText[introStep][2]:len()

            end
        end

        clicking = m.isDown('l')

    end

end


-- On state draw
function introState:draw()

    local text = introText[introStep][2]

    local yOffset = 0
    if isOUYA then yOffset = -16 end
    g.setColor(255, 255, 255)
    g.draw(gfx.introSlate, 0, 16 + yOffset)

    if not isOUYA then
        -- Draw bottom bar
        g.setColor(0, 0, 0)
        g.rectangle('fill', 0, 160, 256, 32)
    else
        g.setColor(0, 0, 0, 192)
        g.rectangle('fill', 0, 112, 256, 32)
    end

    -- Draw intro text
    g.setColor(255, 255, 255)
    g.setFont(fnt.small)
    g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')
    if isOUYA then
        g.setFont(fnt.smallre)
        g.setColor(0, 0, 0)
        g.printf(text:sub(1, math.ceil(introShow)), 2, 162 + yOffset * 3, 252, 'left')
        g.setColor(255, 255, 255)
    end

    if fade then

        if fade >= 0.5 then

            states.play:draw()
        end

        g.setColor(255, 255, 255, 255 - math.abs(fade - 0.5) * 510)
        g.rectangle('fill', 0, 16 + yOffset, 256, 144)

    end

end


-- On state kill
function introState:kill()

    introObject = nil

    introText   = nil

end


-- Transfer data to state loading script
return introState
