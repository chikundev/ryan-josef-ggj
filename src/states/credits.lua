-- chikun :: 2014
-- Credits state


-- Temporary state, removed at end of script
local creditsState = { }


-- On state create
function creditsState:create()

    sfx.credits:play()

    timer = 0

    credits = {
        { "Produced by", "chikun" },
        { "Developed for", "Global Game Jam 2015" },
        { "Designer & Creator", "Ryan Liddle" },
        { "Developer", "Josef Frank" },
        { "Special Thanks", "Chris Alderton" },
        { "Special Thanks", "Keiran Dawson" },
        { "Special Thanks", "Cohen Dennis" },
        { "Special Thanks", "Mathew Dwyer" },
        { "Special Thanks", "Mark Moore" },
        { "Special Thanks", "Bradley Roope" },
    }

end


-- On state update
function creditsState:update(dt)

    timer = timer + dt

    if timer >= 37 then

        e.quit()

    end
end


-- On state draw
function creditsState:draw()

    g.setColor(255, 255, 255)

    if timer >= 7 then

        local val = math.ceil((timer - 7) / 3)

        g.draw(gfx["ry" .. val], 256 - 90 - 16, 16)

        g.setFont(fnt.smallre)

        g.printf(credits[val][1], 0, 84, 150, 'center')

        g.setFont(fnt.small)

        g.printf(credits[val][2], 0, 96, 150, 'center')

    end

end


-- On state kill
function creditsState:kill()

end


-- Transfer data to state loading script
return creditsState
