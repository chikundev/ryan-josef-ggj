roomNames = {
    startRoom = "The Cosmic Door",
    room1 = "The Marble Room",
    room2 = "The Viewing Room",
    room3 = "The Faceless Room",
    room4 = "The Superjoy Room",
    room5 = "The Fire Room",
    room6 = "The Infinite Room",
    room7 = "The Medicine Room",
    room8 = "The Yirrayanlong Room",
    room9 = "The Offering Room",
    room10 = "The Humid Room",
    room11 = "The Windows Room",
    room12 = "The Penultimate Room",
    room13 = "The Singularity",
}

roomCompleted = { }

for key, value in pairs(maps) do

    roomCompleted[value] = false

end

roomBGM = {

    startRoom = "startRoom",
    room1 = "01",
    room2 = "02",
    room3 = "03",
    room4 = "04",
    room5 = "05",
    room6 = "06",
    room7 = "07",
    room8 = "08",
    room9 = "09",
    room10 = "10",
    room11 = "11",
    room12 = "12",
    room13 = "13",

}



roomButtons = {
    {
        name = "room11",
        x = 97,
        y = 76 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room12",
        x = 119,
        y = 76 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room13",
        x = 141,
        y = 76 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room6",
        x = 75,
        y = 98 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room7",
        x = 97,
        y = 98 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room8",
        x = 119,
        y = 98 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room9",
        x = 141,
        y = 98 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room10",
        x = 163,
        y = 98 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room3",
        x = 97,
        y = 120 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room4",
        x = 119,
        y = 120 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room5",
        x = 141,
        y = 120 - 22,
        w = 18,
        h = 18
    },
    {
        name = "startRoom",
        x = 97,
        y = 142 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room1",
        x = 119,
        y = 142 - 22,
        w = 18,
        h = 18
    },
    {
        name = "room2",
        x = 141,
        y = 142 - 22,
        w = 18,
        h = 18
    }
}
