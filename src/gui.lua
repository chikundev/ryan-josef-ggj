gui = { }


gui.create = function()

    hoverText = ""

    talk = nil

    toolButtons = {
        {
            name = "interact",
            text = "Interact",
            x = 4,
            y = 164,
            w = 80,
            h = 24
        },
        {
            name = "examine",
            text = "Examine",
            x = 88,
            y = 164,
            w = 80,
            h = 24
        },
        {
            name = "use",
            text = "Use",
            x = 172,
            y = 164,
            w = 80,
            h = 24
        }
    }

    mapButton = {
        name = "map",
        text = "Map",
        x = 224,
        y = 0,
        w = 16,
        h = 16
    }

    menuButton = {
        name = "menu",
        text = "Menu",
        x = 240,
        y = 0,
        w = 16,
        h = 16
    }

end


gui.update = function(dt, singleClick)

    -- Check hovering of tools
    for key, tool in ipairs(toolButtons) do

        if math.overlap(guiCursor, tool) then

            hoverText = tool.text

            -- If clicking on tool...
            if singleClick then

                sfx.click:play()

                if tool.text ~= "Use" then

                    currentTool = tool.name

                else

                    state.load(states.inventory)

                end

            end
        end
    end


    -- Hovering of menu button
    if math.overlap(guiCursor, menuButton) then

        hoverText = menuButton.text

        if singleClick then

            state.load(states.menu)

        end
    elseif math.overlap(guiCursor, mapButton) then

        hoverText = mapButton.text

        if singleClick then

            state.load(states.map)

        end
    end
end


gui.draw = function()


    if not isOUYA then

        -- Draw black bars
        g.setColor(0, 0, 0)
        g.rectangle('fill', 0, 160, 256, 32)

    else

        g.setColor(0, 0, 0, 192)

    end
    g.rectangle('fill', 0, 0, 256, 16)

    -- Draw hover text
    g.setFont(fnt.small)
    g.setColor(255, 255, 255)
    g.print(hoverText, 2, 2)
    g.setFont(fnt.smallre)
    g.setColor(0, 0, 0)
    g.print(hoverText, 2, 2)
    g.setFont(fnt.small)

    if not isOUYA then

        -- Draw tool buttons
        g.setColor(255, 255, 255)
        for key, tool in ipairs(toolButtons) do

            g.draw(gfx.cursor[tool.name], tool.x + 2, tool.y + 4)

            g.print(tool.text, tool.x + 20, tool.y + 6)

            g.rectangle('line', tool.x, tool.y, tool.w, tool.h)

        end

        g.draw(gfx.icons.map, mapButton.x, mapButton.y)
        g.draw(gfx.icons.settings, menuButton.x, menuButton.y)

    end

    -- If someone is talking
    if (talk) then

        -- Draw text speaker
        g.setColor(talk.colour)
        g.setFont(fnt.small)
        g.print(talk.speaker, 2, 146)
        g.setColor(0, 0, 0)
        g.setFont(fnt.smallre)
        g.print(talk.speaker, 2, 146)

        -- Draw text
        g.setColor(255, 255, 255)
        g.setFont(fnt.small)
        g.printf(talk.text, 2, 162, 252, 'left')

    else



    end

end
