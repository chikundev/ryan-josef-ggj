item = { }

itemList = { }

item.add = function(itemName, held, used)

    table.insert(itemList,{
            name    = itemName,
            held    = held or false,
            used    = used or false
        })

end


item.collect = function(itemName)

    for key, singleItem in ipairs(itemList) do

        if singleItem.name == itemName then

            singleItem.held = true

        end
    end
end


item.check = function(itemName)

    for key, singleItem in ipairs(itemList) do

        if singleItem.name == itemName and singleItem.held then

            return true

        end
    end

    return false
end


item.modify = function(oldName, newName)

    for key, singleItem in ipairs(itemList) do

        if singleItem.name == oldName then

            singleItem.name = newName

        end
    end
end


item.getUsed = function(itemName)

    for key, singleItem in ipairs(itemList) do

        if singleItem.name == itemName then

            return singleItem.used

        end
    end
end


item.setUsed = function(itemName)

    for key, singleItem in ipairs(itemList) do

        if singleItem.name == itemName then

            singleItem.used = true

        end
    end
end



item.add('duck')
item.add('pitcher')
item.add('cocaine')
item.add('crucifix')
item.add('dogFood')
item.add('unholyLiquid')
item.add('key1')
item.add('key2')
