-- Test talking

return {
    {
        bgImage = gfx.dialog.turtle,
        speaker = "turtle",
        text    = "The Zoroastrian libation rites.",
    },
    {
        speaker = "turtle",
        text    = "Wine is poured in four glasses, then a blessing is recieved.",
        finish  = true,

    },
}
