-- Test talking

return {
    {
        bgImage = gfx.dialog.medicineBuddha,
        speaker = "medicineBuddha",
        text    = "In peace and war, sickness and health,"
    },
     {
        speaker = "medicineBuddha",
        text    = "there is one thing that persists.",
    },
    {
        speaker = "medicineBuddha",
        text    = "Do you know what 'it' is?",
        choices = {
            { "Yes", function()
                    diaStep = 4
                    diaShow = 0
                end },
            { "No", function()
                    diaStep = 5
                    diaShow = 0
                end }
        }
    },
    {
        speaker = "medicineBuddha",
        text    = "Acceptance is the first stage of self-realisation.",
        next = 6
    },

    {
        speaker = "medicineBuddha",
        text    = "That is dukkha: suffering. But it can be resisted.  ",
        next = 6
    },

    {
        speaker = "medicineBuddha",
        text    = "The future holds terrible things.",
    },
    {
        speaker = "medicineBuddha",
        text    = "That is why we must persist in the present.",
    },
    {
        speaker = "medicineBuddha",
        text    = "Now is the only time that matters.",
    },
    {
        speaker = "medicineBuddha",
        text    = "Remember this.",
    },
    {
        speaker = "medicineBuddha",
        text    = "And take this sacred pitcher with you - it purifies all it touches.",
        code    = function()
                medicineBuddha1 = true
                item.collect('pitcher')
            end,
        finish  = true
    },
}
