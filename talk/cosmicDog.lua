-- Test talking

return {
        {
        bgImage = gfx.dialog.cosmicDog,
        speaker = "cosmicDog",
        text    = "Woof!",
        code    = function()

           sfx.woof:play()

        end
        },
        {
        speaker = "blank",
        text    = "Despite the fact that Cosmic Dog is enlightened -",
        },
        {
        speaker = "blank",
        text    = "- you remind yourself that Cosmic Dog is just a dog.",
        finish  = true
        },
}
