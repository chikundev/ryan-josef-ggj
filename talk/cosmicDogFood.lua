-- Test talking

return {
        {
        bgImage = gfx.dialog.cosmicDog,
        speaker = "cosmicDog",
        text    = "Woof!",
        code    = function()

           sfx.woof:play()

        end
        },
        {
        speaker = "blank",
        text    = "The dog drops a key whilst eating your dog food."
        },
        {
        speaker = "blank",
        text    = "You may as well take it.",
        code    = function()
            cosmicDogFood = true
            item.collect('key1')
        end,
        finish  = true
        },
}
