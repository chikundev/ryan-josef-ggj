-- Test talking

return {
    {
        bgImage = gfx.dialog.shiva,
        speaker = "shiva",
        text    = "Cycles begin and end. Worlds phase into and outside of my existence."
    },
     {
        speaker = "shiva",
        text    = "I am Shiva, creator and destroyer of the cosmos.",
    },
    {
        speaker = "shiva",
        text    = "Life comes and passes. That is part of the Cycle.",
    },
    {
        speaker = "shiva",
        text    = "But so is the Beginning and the End of time.",
    },
    {
        speaker = "shiva",
        text    = "What does one minute, one week, one year mean...",
    },
    {
        speaker = "shiva",
        text    = "...When I see galaxies form and dissipate?.",
    },
    {
        speaker = "shiva",
        text    = "What use is time itself?.",
        finish  = true
    },
}
