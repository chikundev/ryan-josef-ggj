-- Test talking

return {
    {
        bgImage = gfx.dialog.duck,
        speaker = "duck",
        text    = "Do not be afraid. I am not a rubber duck.",
    },
     {
        bgImage = gfx.dialog.duck,
        speaker = "duck",
        text    = "The ripples in the fabric of reality have wrought hideous things -",
    },
    {
        speaker = "duck",
        text    = " - upon our world and its inhabitants.",
    },
    {
        speaker = "duck",
        text    = " I fear soon the Big Rip will tear our cosmic Cycle apart.",
    },
     {
        speaker = "duck",
        text    = "You are still normal. You must find a way.",
    },
     {
        speaker = "duck",
        text    = "You are the last hope of humanity, nay, the universe itself.",
    },
    {
        speaker = "duck",
        text    = "T-take me with you.",
        code    = function()
            for key, layer in ipairs(map.current.layers) do

                if layer.name == "objects" then

                    for key, value in ipairs(layer.objects) do

                        if value.name == "duck" then

                            table.remove(layer.objects, key)

                        end
                    end
                end
            end
            item.collect('duck')
        end,
        finish  = true
    }
}

