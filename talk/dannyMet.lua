-- Test talking

return {
    {
        bgImage = gfx.dialog.danny,
        speaker = "danny",
        text    = "You must complete the Unholy Ritual and bestow me the Curse.",
    },
    {
        speaker = "danny",
        text    = "With this, I may transcend Space, Time, and Form.",
    },
    {
        speaker = "danny",
        text    = "Only then may I help you. ",
        finish  = true
    },
}
