-- Test talking

return {
    {
        bgImage = gfx.dialog.mask1,
        speaker = "maskOfAbassi",
        text    = "A thing is there whose voice is one, whose feet are four and two.",
        choices = {
            { "Pastoralism", function()
                    diaStep = 2
                    diaShow = 0
                    spiritHappy1 = true
                end },
            { "Urbanism", function()
                    diaStep = 3
                    diaShow = 0
                    spiritsAngry = true
                end }
        }
    },
    {
        speaker = "maskOfAbassi",
        text    = "Yes, that is correct.",
        finish  = true
    },
    {
        speaker = "maskOfAbassi",
        text    = "...",
        finish  = true
    }
}
