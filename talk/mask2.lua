-- Test talking

return {
    {
        bgImage = gfx.dialog.mask2,
        speaker = "maskOfYemaya",
        text    = "There is a house. One enters it blind, and leaves it seeing."
    },
    {
        speaker = "maskOfYemaya",
        text    = "What is it?",
        choices = {
            { "A hospital.", function()
                    diaStep = 3
                    diaShow = 0
                    spiritsAngry = true
                end },
            { "A school.", function()
                    diaStep = 4
                    diaShow = 0
                    spiritHappy2 = true
                end }
        }
    },
    {
        speaker = "maskOfYemaya",
        text    = "...",
        finish  = true
    },
    {
        speaker = "maskOfYemaya",
        text    = "Yes. That is correct.",
        finish  = true
    }
}
