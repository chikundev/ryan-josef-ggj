-- Test talking

return {
        {
        bgImage = gfx.dialog.time,
        speaker = "blank",
        text    = "A map with the current time across the world.",
        },
        {
        speaker = "blank",
        text    = "Since recent events, 'time' has become pointless.",
        },
        {
        speaker = "blank",
        text    = "The world is amalgamating into a singularity of time and space.",
        },
        {
        speaker = "blank",
        text    = "Time and space are no longer relative.",
        finish  = true
        },
}
