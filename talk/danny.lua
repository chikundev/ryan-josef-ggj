-- Test talking

return {
    {
        bgImage = gfx.dialog.danny,
        speaker = "danny",
        text    = "It's time. Everything - the universe itself...",
    },
    {
        speaker = "danny",
        text    = "...Is set to split apart. And I am ready to show my hand.",
    },
    {
        speaker = "danny",
        text    = "I need you. You and I have a common goal.",
    },
    {
        speaker = "danny",
        text    = "Death. Or more so, its avoidance.",
    },
    {
        speaker = "danny",
        text    = "As realities converge the mysteries of the world illuminate.",
    },
    {
        speaker = "danny",
        text    = "There is a way to end this absurdity, but your help is needed.",
    },
    {
        speaker = "danny",
        text    = "I know of a ritual, an ancient, esoteric Curse...",
    },
    {
        speaker = "danny",
        text    = "A Curse that grants a person release from entropy... ",
    },
    {
        speaker = "danny",
        text    = ".... at a cost. ",
    },
    {
        speaker = "danny",
        text    = "Perform it upon me, and I shall become one with the Cycle.",
    },
     {
        speaker = "danny",
        text = "And I will give you passage into the fabric of space-time itself.",
         finish  = true
    },
}
