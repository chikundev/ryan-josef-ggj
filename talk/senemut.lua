-- Test talking

return {
    {
        bgImage = gfx.dialog.senemut,
        speaker = "senemut",
        text    = "The Unholy Curse."
    },
     {
        speaker = "senemut",
        text    = "In this circle, three reagants.",
    },
    {
        speaker = "senemut",
        text    = "One thing that changes the mind.",
    },
    {
        speaker = "senemut",
        text    = "One thing that changes the body.",
    },
      {
        speaker = "senemut",
        text    = "One thing that changes the soul.",
    },
     {
        speaker = "senemut",
        text    = "These things must be burnt in the Circle.",
    },
    {
        speaker = "senemut",
        text    = "And, in its place, an Everlasting Curse...",
    },
    {
        speaker = "senemut",
        text    = "Those formless exposed will suffer life eternal, to insanity.",
    },
}
