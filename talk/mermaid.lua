-- Test talking

return {
    {
        bgImage = gfx.dialog.mermaid,
        speaker = "mermaid",
        text    = "What would you sacrifice to be happy with your life?",
    },
    {
        speaker = "mermaid",
        text    = "Is happiness a matter of doing, or doing without?",
        finish  = true,
        code = function()
            merCheck1 = true
        end
    }
}
