-- Test talking

return {
    {
        bgImage = gfx.dialog.danny,
        speaker = "danny",
        text    = "It is complete! The ritual nears its end!",
    },
    {
        speaker = "danny",
        text    = "G-give it to me! The elixr! I crave it!",
    },
    {
        speaker = "danny",
        text    = "Hngh...",
        code    = function()
            bgm["02"]:stop()
        end
    },
    {
        bgImage = gfx.dialog.danny2,
        speaker = "danny",
        text    = "Hahaha! I am entering the subatomic matrix...",

        code = function()
            sfx.violin:play()
        end
    },
    {
        speaker = "danny",
        text    = "My mind entangles with the quantum fabric!",
    },
    {

        speaker = "danny",
        text    = "I'm becoming godlike. I'm becoming a fucking GOD.",
    },
    {
        speaker = "danny",
        text    = "Ahah-hahahahahaha! Hah! Ha! Ha! Hahhhhhh!",
    },
    {
        bgImage = gfx.dialog.danny3,
        speaker = "danny",
        text    = "...",
    },
    {
        speaker = "blank",
        text    = "Danny Elfman has disappeared from the painting.",
    },
    {
        bgImage = gfx.dialog.danny3,
        speaker = "danny",
        text    = "A voice in your head murmurs - 'Go to Yirrayanlong'.",
        finish  = true,
        code    = function()

            dannyMet2 = true

            for key, layer in ipairs(map.current.layers) do

                if layer.name == "objects" then

                    for key, value in ipairs(layer.objects) do

                        if value.name == "dannyFrame" then

                            table.remove(layer.objects, key)

                        end
                    end
                end
            end

            for key, layer in ipairs(map.current.layers) do

                if layer.name == "objects" then

                    for key, value in ipairs(layer.objects) do

                        if value.name == "dannyBlank" then

                            value.name = "dannyFrame"

                        end
                    end
                end
            end

        end
    }

}
