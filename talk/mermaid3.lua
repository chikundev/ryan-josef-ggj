-- Test talking

return {
    {
        bgImage = gfx.dialog.mermaid3,
        speaker = "mermaid3",
        text    = "Beware the Faceless Room. They are spirits who speak in riddles.",
    },
    {
        speaker = "mermaid3",
        text    = "They are vengeful spirits..",
        finish  = true,
        code = function()
            merCheck3 = true
        end
    },
}
