-- Test talking

return {
    {
        bgImage = gfx.dialog.statue,
        speaker = "statue",
        text    = "I see blinding flashes of light emanate from you.",
    },
    {
        speaker = "statue",
        text    = "Brilliant and luminous.",
    },
    {
        speaker = "statue",
        text    = "The cosmic disruption dissipates when it occurs.",
        finish  = true
    },
}
