-- Test talking

return {
    {
        bgImage = gfx.dialog.chameleon,
        speaker = "chameleon",
        text    = "..."
    },
    {
        speaker = "blank",
        text    = "The chameleon is holding a bag of dog food.",
    },
     {
        speaker = "blank",
        text    = "It's definitely a chameleon, and not a dog.",
        },
     {
        speaker = "blank",
        text    = "The chameleon probably stole the bag.",
        },
         {
        speaker = "blank",
        text    = "You confiscate the bag of dog food from the chameleon.",
        code    = function()

            chameleon1 = true
            item.collect('dogFood')

        end,
        finish  = true
    },
}
