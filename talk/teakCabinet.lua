-- Test talking

return {
    {
        bgImage = gfx.dialog.teakCabinet,
        speaker = "teakCabinet",
        text    = "It's a suspiciously small teak cabinet."
    },
    {
        speaker = "teakCabinet",
        text    = "Open it?",
        choices = {
            { "Yes", function()
                    diaStep = 4
                    diaShow = 0
                end },
            { "No", function()
                    diaStep = 3
                    diaShow = 0
                end }
        }
    },
    {
        speaker = "teakCabinet",
        text    = "You leave the teak cabinet unmolested.",
        finish = true
    },

    {
        speaker = "teakCabinet",
        text    = "You decide to open the teak cabinet.",
    },

    {
        bgImage = gfx.dialog.peeweeCabinet1,
        speaker = "teakCabinet",
        text    = "Paul Reubens; Pee-Wee Herman is hiding inside the tiny teak cabinet.",
    },
    {
        speaker = "peeweeCabinet",
        text    = "Ok, you found me.",
        code = function()
            peewee = true
            end
    },
    {
        speaker = "peeweeCabinet",
        text    = "Um, listen, I don't wanna get caught lurking around here.",
    },
    {
        speaker = "peeweeCabinet",
            bgImage = gfx.dialog.peeweeCabinet2,

        text    = "If you don't tell anybody, I'll give you drugs.",
    },
    {
        speaker = "peeweeCabinet",
        text    = "Pure crack cocaine.",
    },
    {
        speaker = "peeweeCabinet",
        text    = "You can trust Ol' Pee-Wee, right?.",
        choices = {
            { "Yes", function()
                    diaStep = 11
                    diaShow = 0
                end },
            { "Yes", function()
                    diaStep = 11
                    diaShow = 0
                end }
        },
    },
    {
        speaker = "peeweeCabinet",
        text    = "(Pee-Wee gives you the cocaine.)",
        code    = function()
            item.collect('cocaine')
        end
    },
    {
        speaker = "peeweeCabinet",
        text    = "(He returns to his inconspicuous den.)"
    },
}
