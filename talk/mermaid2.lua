-- Test talking

return {
    {
        bgImage = gfx.dialog.mermaid2,
        speaker = "mermaid2",
        text    = "Big Rip. Guaranteed entropy.",
    },
    {
        speaker = "mermaid2",
        text    = "The Cycle is ending. I am scared.",
        finish  = true,
        code = function()
            merCheck2 = true
        end
    },
}
