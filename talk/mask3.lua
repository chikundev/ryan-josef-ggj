-- Test talking

return {
    {
        bgImage = gfx.dialog.mask3,
        speaker = "maskOfOlorun",
        text = "What we caught, we threw away; what we didn't catch, we kept."
    },
    {
        speaker = "maskOfOlorun",
        text    = "What was kept?",
        choices = {
            { "Lice", function()
                    diaStep = 3
                    diaShow = 0
                    spiritHappy3 = true
                end },
            { "Illness", function()
                    diaStep = 4
                    diaShow = 0
                    spiritsAngry = true
                end }
        }
    },
    {
        speaker = "maskOfOlorun",
        text    = "Yes, that is correct.",
        finish  = true
    },
    {
        speaker = "maskOfOlorun",
        text    = "...",
        finish  = true
    }
}
