-- Test talking

return {
    {
        bgImage = gfx.background1,
        speaker = "danny",
        text    = "This is a test",
        choices = {
            { "Hate", function()
                    diaStep = 2
                    diaShow = 0
                end },
            { "Love", function()
                    diaStep = 3
                    diaShow = 0
                end }
        }
    },
    {
        speaker = "danny",
        text    = "Well OK then",
        finish  = true
    },
    {
        speaker = "danny",
        text    = "Hmm"
    },
    {
        speaker = "danny",
        text    = "Bullshit",
        finish  = true
    }
}
