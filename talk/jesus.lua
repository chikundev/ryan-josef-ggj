-- Test talking

return {
    {
        bgImage = gfx.dialog.jesus,
        speaker = "blank",
        text    = "(The statue emanates a booming voice.)"
    },
    {
        speaker = "blank",
        text    = "I Am A Statue Of Jesus.",
    },
    {
        speaker = "jesus",
        text    = "I Know Why You Are Here. Do You?",
        choices = {
            { "Yes", function()
                    diaStep = 4
                    diaShow = 0
                end },
            { "No", function()
                    diaStep = 6
                    diaShow = 0
                end }
        }
    },
    {
        speaker = "jesus",
        text    = "Then You Will Know This Is Not The Eschatological Rapture",
    },
    {
        speaker = "jesus",
        text    = "But This Is Nonetheless Something Quite Beyond Your Understanding.",
        next = 7,
    },
   {
        speaker = "jesus",
        text    = "What Occurs Now Is Something Beyond Your Understanding.",
    },
    {
        speaker = "jesus",
        text    = "But Do Not Fear. Take The Amulet From My Pedestal.",
    },
    {
        speaker = "jesus",
        text    = "It Will Come In More Use Than You May Believe.",
        code = function()
            jesus1 = true
            item.collect('crucifix')

         end
    },
    {
        speaker = "blank",
        text    = "(The statue's booming voice becomes silent.)",
    },
}
