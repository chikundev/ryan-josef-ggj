-- Test talking

return {
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "You came here to preserve the Cycle. I have found a solution.",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "You see, we reach the event horizon,",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "and time has slowed but to a halt...",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "Why not make that permanent?",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "The powers I have attained give me power over life and death.",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "And, I will use them to slow time to the smallest increment.",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "An Endless Now.",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "Time is an illusion itself. Seconds, years, is it not relative?",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "This distinction will save myself from destruction.",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "But I am afraid there are also many things that must be erased.",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "After all, the Endless Now is endless.",
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "Why on earth would I spend it with this world?"
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "Though I would save you, being my vehicle to immortality..."
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "I am afraid you cannot be spared."
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "dannyWorld",
        text    = "Let the Endless Now begin."
    },
    {
        bgImage = gfx.blank,
        speaker = "blank",
        text    = "..."
    },
    {
        bgImage = gfx.dialog.dannyGod1,
        speaker = "blank",
        text    = "In the beginning, God created the heavens and the earth. ",
    },
    {
        bgImage = gfx.dialog.dannyGod2,
        speaker = "blank",
        text    = "And God said, \"Let there be light.\" And there was light.",
    },
    {
        bgImage = gfx.dialog.dannyGod3,
        speaker = "blank",
        text    = "And God said, \"Let the water teem with living creatures.\"",
    },
    {
        bgImage = gfx.dialog.dannyGod3,
        speaker = "blank",
        text    = "So God created the great creatures of the sea.",
    },
    {
        bgImage = gfx.dialog.dannyGod4,
        speaker = "blank",
        text    = "And God said, \"Let the land produce living creatures\". And it was so.",
    },
    {
        bgImage = gfx.dialog.dannyGod5,
        speaker = "blank",
        text    = "Then God said, \"Let us make mankind in our image, in our likeness.",

    },
    {
            bgImage = gfx.dialog.dannyGod1,
        speaker = "blank",
        text    = "God saw all that he had made...",
     },
    {
        bgImage = gfx.blank,
        speaker = "blank",
        text    = "And it was very good.",
    },
    {
        bgImage = gfx.blank,
        speaker = "blank",
        text    = "",
        code    = function()
            state.change(states.credits)
        end,
        finish  = true,
    },
}
