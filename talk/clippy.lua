-- Test talking

return {
    {
        bgImage = gfx.dialog.clippy,
        speaker = "clippy",
        text    = "The world is becoming increasingly unstable.",
    },
    {
        speaker = "clippy",
        text    = "Worlds converge, no matter how real or imagined.",
    },
    {
        speaker = "clippy",
        text    = "I am afraid the end is near.",
        code = function()
            clippyTalk = true
        end
    },
}
