return {
  version = "1.1",
  luaversion = "5.1",
  orientation = "orthogonal",
  width = 32,
  height = 9,
  tilewidth = 16,
  tileheight = 16,
  properties = {},
  tilesets = {
    {
      name = "mockintFull",
      firstgid = 1,
      tilewidth = 512,
      tileheight = 144,
      spacing = 0,
      margin = 0,
      image = "../gfx/mockintFull.png",
      imagewidth = 512,
      imageheight = 144,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      tiles = {}
    },
    {
      name = "table",
      firstgid = 2,
      tilewidth = 64,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/table.png",
      imagewidth = 64,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      tiles = {}
    },
    {
      name = "door",
      firstgid = 3,
      tilewidth = 32,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "../gfx/door.png",
      imagewidth = 32,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      tiles = {}
    },
    {
      name = "doorRight",
      firstgid = 4,
      tilewidth = 32,
      tileheight = 96,
      spacing = 0,
      margin = 0,
      image = "../gfx/doorRight.png",
      imagewidth = 32,
      imageheight = 96,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      tiles = {}
    },
    {
      name = "doorLeft",
      firstgid = 5,
      tilewidth = 32,
      tileheight = 96,
      spacing = 0,
      margin = 0,
      image = "../gfx/doorLeft.png",
      imagewidth = 32,
      imageheight = 96,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      tiles = {}
    },
    {
      name = "clocksmall",
      firstgid = 6,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "../gfx/clocksmall.png",
      imagewidth = 16,
      imageheight = 16,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 32,
      height = 9,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "objects",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "table",
          type = "",
          shape = "rectangle",
          x = 152,
          y = 131.333,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 2,
          visible = true,
          properties = {
            ["hoverText"] = "Table"
          }
        },
        {
          name = "doorBack",
          type = "",
          shape = "rectangle",
          x = 324,
          y = 126,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 3,
          visible = true,
          properties = {
            ["hoverText"] = "Door to Kitchen"
          }
        },
        {
          name = "doorRight",
          type = "",
          shape = "rectangle",
          x = 479,
          y = 149,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 4,
          visible = true,
          properties = {
            ["hoverText"] = "Door to Bedroom"
          }
        },
        {
          name = "doorLeft",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 149,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 5,
          visible = true,
          properties = {
            ["hoverText"] = "Door to Outside"
          }
        }
      }
    },
    {
      type = "objectgroup",
      name = "objects",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "clock",
          type = "",
          shape = "rectangle",
          x = 176,
          y = 100.5,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 6,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "important",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "playerStart",
          type = "",
          shape = "rectangle",
          x = 28.3333,
          y = 110,
          width = 12,
          height = 24,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
