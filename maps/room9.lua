return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "2015-01-20",
  orientation = "orthogonal",
  width = 32,
  height = 9,
  tilewidth = 16,
  tileheight = 16,
  nextobjectid = 28,
  properties = {},
  tilesets = {
    {
      name = "background9",
      firstgid = 1,
      tilewidth = 512,
      tileheight = 144,
      spacing = 0,
      margin = 0,
      image = "../gfx/background9.png",
      imagewidth = 512,
      imageheight = 144,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "senemut",
      firstgid = 2,
      tilewidth = 58,
      tileheight = 77,
      spacing = 0,
      margin = 0,
      image = "../gfx/senemut.png",
      imagewidth = 58,
      imageheight = 77,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "door6",
      firstgid = 3,
      tilewidth = 32,
      tileheight = 96,
      spacing = 0,
      margin = 0,
      image = "../gfx/door6.png",
      imagewidth = 32,
      imageheight = 96,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "door6flipped",
      firstgid = 4,
      tilewidth = 32,
      tileheight = 96,
      spacing = 0,
      margin = 0,
      image = "../gfx/door6flipped.png",
      imagewidth = 32,
      imageheight = 96,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 32,
      height = 9,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "objects",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 2,
          name = "senemut",
          type = "",
          shape = "rectangle",
          x = 223.333,
          y = 113,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          id = 23,
          name = "door6",
          type = "",
          shape = "rectangle",
          x = 134,
          y = 140.333,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 3,
          visible = true,
          properties = {}
        },
        {
          id = 25,
          name = "circle",
          type = "",
          shape = "rectangle",
          x = 208.909,
          y = 114.909,
          width = 96.1818,
          height = 26.3636,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 27,
          name = "door7",
          type = "",
          shape = "rectangle",
          x = 340.333,
          y = 139,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 4,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "objects",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {}
    },
    {
      type = "objectgroup",
      name = "important",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 1,
          name = "playerStart",
          type = "",
          shape = "rectangle",
          x = 176.458,
          y = 94.7083,
          width = 15.75,
          height = 31.25,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "collisions",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 17,
          name = "",
          type = "",
          shape = "rectangle",
          x = 111.333,
          y = 54.3333,
          width = 28.6667,
          height = 104.667,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 18,
          name = "",
          type = "",
          shape = "rectangle",
          x = 367,
          y = 39.6667,
          width = 40,
          height = 119.333,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
