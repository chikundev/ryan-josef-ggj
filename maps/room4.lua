return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.11.0",
  orientation = "orthogonal",
  width = 32,
  height = 9,
  tilewidth = 16,
  tileheight = 16,
  nextobjectid = 31,
  properties = {},
  tilesets = {
    {
      name = "background4",
      firstgid = 1,
      tilewidth = 512,
      tileheight = 144,
      spacing = 0,
      margin = 0,
      image = "../gfx/background4.png",
      imagewidth = 512,
      imageheight = 144,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "door2Back",
      firstgid = 2,
      tilewidth = 32,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "../gfx/door2Back.png",
      imagewidth = 32,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "door2Left",
      firstgid = 3,
      tilewidth = 32,
      tileheight = 96,
      spacing = 0,
      margin = 0,
      image = "../gfx/door2Left.png",
      imagewidth = 32,
      imageheight = 96,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "door2Right",
      firstgid = 4,
      tilewidth = 32,
      tileheight = 96,
      spacing = 0,
      margin = 0,
      image = "../gfx/door2Right.png",
      imagewidth = 32,
      imageheight = 96,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "door2Forward",
      firstgid = 5,
      tilewidth = 32,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "../gfx/door2Forward.png",
      imagewidth = 32,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "teakCabinet",
      firstgid = 6,
      tilewidth = 43,
      tileheight = 47,
      spacing = 0,
      margin = 0,
      image = "../gfx/teakcabinet.png",
      imagewidth = 43,
      imageheight = 47,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "plant1",
      firstgid = 7,
      tilewidth = 32,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "../gfx/plant1.png",
      imagewidth = 32,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    },
    {
      name = "turtle",
      firstgid = 8,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/turtle.png",
      imagewidth = 32,
      imageheight = 32,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 32,
      height = 9,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 0, 0, 0, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "objects",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 15,
          name = "door2Back",
          type = "",
          shape = "rectangle",
          x = 422.5,
          y = 126,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          id = 18,
          name = "door2Left",
          type = "",
          shape = "rectangle",
          x = -0.5,
          y = 147,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 3,
          visible = true,
          properties = {}
        },
        {
          id = 19,
          name = "door2Right",
          type = "",
          shape = "rectangle",
          x = 481.5,
          y = 146,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 4,
          visible = true,
          properties = {}
        },
        {
          id = 20,
          name = "door2Forward",
          type = "",
          shape = "rectangle",
          x = 61,
          y = 127,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 5,
          visible = true,
          properties = {}
        },
        {
          id = 24,
          name = "teakCabinet",
          type = "",
          shape = "rectangle",
          x = 302,
          y = 131,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 6,
          visible = true,
          properties = {}
        },
        {
          id = 30,
          name = "turtle",
          type = "",
          shape = "rectangle",
          x = 180,
          y = 132,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 8,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "objects",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {}
    },
    {
      type = "objectgroup",
      name = "important",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 17,
          name = "playerStart",
          type = "",
          shape = "rectangle",
          x = 216.125,
          y = 106.875,
          width = 15.75,
          height = 31.25,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "plants",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 27,
          name = "",
          type = "",
          shape = "rectangle",
          x = 352,
          y = 128,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 7,
          visible = true,
          properties = {}
        },
        {
          id = 29,
          name = "",
          type = "",
          shape = "rectangle",
          x = 260,
          y = 128,
          width = 0,
          height = 0,
          rotation = 0,
          gid = 7,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
