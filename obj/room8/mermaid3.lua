-- chikun :: 2015
-- Clock object in test room


-- Table which holds table object
local mermaid3 = {
    text    = "?",
    cursor  = "interact",
    examine = {
        { "", "An unidentifiable sculpture." }
    }
}


-- Performed when player needs to know where to go
function mermaid3:getOffset()

    return 33, 100

end


-- Performed when player arrives at object after object is clicked on
function mermaid3:onTouch()

    triggerDialogue("mermaid3")

end


-- Add clock to the bunch
return mermaid3
