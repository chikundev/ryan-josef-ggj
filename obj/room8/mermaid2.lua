-- chikun :: 2015
-- Clock object in test room


-- Table which holds table object
local mermaid2 = {
    text    = "?",
    cursor  = "interact",
    examine = {
        { "", "An unidentifiable sculpture." }
    }
}


-- Performed when player needs to know where to go
function mermaid2:getOffset()

    return 33, 100

end


-- Performed when player arrives at object after object is clicked on
function mermaid2:onTouch()

    triggerDialogue("mermaid2")

end


-- Add clock to the bunch
return mermaid2
