-- chikun :: 2015
-- Clock object in test room


-- Table which holds table object
local mermaid = {
    text    = "?",
    cursor  = "interact",
    examine = {
        { "", "An unidentifiable sculpture." }
    }
}


-- Performed when player needs to know where to go
function mermaid:getOffset()

    return 33, 111

end


-- Performed when player arrives at object after object is clicked on
function mermaid:onTouch()

    triggerDialogue("mermaid")

end


-- Add clock to the bunch
return mermaid
