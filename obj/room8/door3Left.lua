-- chikun :: 2015
-- Object table


-- Table which holds object
local door3Left = {
    text    = "Medicine Room",
    cursor  = "pointLeft"
}


-- Performed when player needs to know where to go
function door3Left:getOffset()

    return  40, 82

end


-- Performed when player arrives at object after object is clicked on
function door3Left:onTouch()

    -- Change to next map and teleport player to 'doorRight'
    map.change(maps.room7, "door4Right")

end


-- Add object to the bunch
return door3Left
