-- chikun :: 2015
-- Object table


-- Table which holds object
local door3Forward = {
    text    = "Unknown",
    cursor  = "interact"
}


-- Performed when player needs to know where to go
function door3Forward:getOffset()

    return  16, 80

end


-- Performed when player arrives at object after object is clicked on
function door3Forward:onTouch()

    if not dannyMet2 then

        launchText({
                { "", "This large door is bolted shut." }
            })
    else

        if not winDoorNew then

            winDoorNew = true

            launchText({
                    { "", "The instability rises. Reality fades away.." }
                })

            obj[map.current.name].door3Forward.name = "Windows Room"

        end

       -- Change to next map and teleport player to 'doorLeft'
        map.change(maps.room11, "door7b")

    end

end


-- Add object to the bunch
return door3Forward
