-- chikun :: 2015
-- Object table


-- Table which holds object
local door3Right = {
    text    = "Offering Room",
    cursor  = "pointRight"
}


-- Performed when player needs to know where to go
function door3Right:getOffset()

    return  -18, 82

end


-- Performed when player arrives at object after object is clicked on
function door3Right:onTouch()

    -- Change to next map and teleport player to 'doorRight'
    map.change(maps.room9, "door6")

end


-- Add object to the bunch
return door3Right
