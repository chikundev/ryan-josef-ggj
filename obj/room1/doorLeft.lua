-- chikun :: 2015
-- Object table


-- Table which holds object
local doorLeft = {
    text    = "Cosmic Door",
    cursor  = "pointLeft"
}


-- Performed when player needs to know where to go
function doorLeft:getOffset()

    return  40, 82

end


-- Performed when player arrives at object after object is clicked on
function doorLeft:onTouch()

    -- Change to next map and teleport player to 'doorRight'
    map.change(maps.startRoom, "doorRight")

end


-- Add object to the bunch
return doorLeft
