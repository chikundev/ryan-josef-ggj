-- chikun :: 2015
-- Clock object in test room


-- Table which holds table object
local jesus = {
    text    = "Statue of a Woman",
    cursor  = "interact",
    examine = {
        { "", "A marble statue of a Greek woman." }
    }
}

-- Performed when player needs to know where to go
function jesus:getOffset()

    return 15, 104

end


-- Performed when player arrives at object after object is clicked on
function jesus:onTouch()

    triggerDialogue("statue")

end


-- Add clock to the bunch
return jesus
