-- chikun :: 2015
-- Clock object in test room


-- Table which holds table object
local jesus = {
    text    = "Statue of Jesus",
    cursor  = "interact",
    examine = {
        { "", "A marble statue of Jesus Christ." }
    }
}


-- Performed when player needs to know where to go
function jesus:getOffset()

    return 49, 132

end


-- Performed when player arrives at object after object is clicked on
function jesus:onTouch()

    if jesus1 then
        launchText ({{ "", "The statue remains silent." }})

    else

    triggerDialogue("jesus")

    end

end


-- Add clock to the bunch
return jesus
