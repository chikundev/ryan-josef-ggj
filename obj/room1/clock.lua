-- chikun :: 2015
-- Clock object in test room


-- Table which holds table object
local clock = {
    text    = "Clock",
    cursor  = "interact",
    examine = {
        { "", "It's a beautiful old clock." },
        { "","Too bad it's made of pain and misery." }
    }
}


-- Performed when player needs to know where to go
function clock:getOffset()

    return 8, 48

end


-- Performed when player arrives at object after object is clicked on
function clock:onTouch()

end


-- Add clock to the bunch
return clock
