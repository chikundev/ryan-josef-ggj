-- chikun :: 2015
-- Object table


-- Table which holds object
local doorForward = {
    text    = "Superjoy Room",
    cursor  = "interact"
}


-- Performed when player needs to know where to go
function doorForward:getOffset()

    return  16, 80

end


-- Performed when player arrives at object after object is clicked on
function doorForward:onTouch()

   -- Change to next map and teleport player to 'doorLeft'
    map.change(maps.room4, "door2Back")

end


-- Add object to the bunch
return doorForward
