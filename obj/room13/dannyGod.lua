-- chikun :: 2015
-- God object in test room


-- Table which holds table object
local dannyGod = {
    text    = "God",
    cursor  = "interact",
    examine = {
        { "", "The ascended form of Danny Elfman." }
    }
}


-- Performed when player needs to know where to go
function dannyGod:getOffset()

    return 60, 130

end


-- Performed when player arrives at object after object is clicked on
function dannyGod:onTouch()

    triggerDialogue("dannyGod")

end


-- Add clock to the bunch
return dannyGod
