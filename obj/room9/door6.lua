-- chikun :: 2015
-- Object table


-- Table which holds object
local door6 = {
    text    = "Yirrayanlong Room",
    cursor  = "pointLeft"
}


-- Performed when player needs to know where to go
function door6:getOffset()

    return  40, 82

end


-- Performed when player arrives at object after object is clicked on
function door6:onTouch()

    -- Change to next map and teleport player to 'doorRight'
    map.change(maps.room8, "door3Right")

end


-- Add object to the bunch
return door6
