-- chikun :: 2015
-- Object table


-- Table which holds object
local circle = {
    text    = "Offering Circle",
    cursor  = "interact",
    examine = {
        { "", "An unadorned offering circle." }
    }
}


-- Performed when player needs to know where to go
function circle:getOffset()

    return  48, 13

end


-- Performed when player arrives at object after object is clicked on
function circle:onTouch()

    launchText({
            { "", "You can not offer yourself." }
        })

end


-- Add object to the bunch
return circle
