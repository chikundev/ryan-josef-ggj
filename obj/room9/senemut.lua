-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local senemut = {
    text    = "Senemut",
    cursor  = "interact",
    examine = {
        {"", "This is Senemut, disgraced consort of Queen Hatshepsut."}
    }
}


-- Performed when player needs to know where to go
function senemut:getOffset()

    return 28, 100

end


-- Performed when player arrives at object after object is clicked on
function senemut:onTouch()

        triggerDialogue("senemut")

end

-- Add candle to the bunch
return senemut
