-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local duck = {
    text    = "Rubber Duck",
    cursor  = "interact",
    examine = {
        {"", "This seems to be a rubber duck in the middle of a desert."}
    }
}


-- Performed when player needs to know where to go
function duck:getOffset()

    return 10, 35

end


-- Performed when player arrives at object after object is clicked on
function duck:onTouch()

    triggerDialogue("duck")

end


-- Add candle to the bunch
return duck
