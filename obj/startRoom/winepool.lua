-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local winepool = {
    text    = "Pool of Red Liquid",
    cursor  = "interact",
    examine = {
        {"", "A pool of red liquid."}
    }
}


-- Performed when player needs to know where to go
function winepool:getOffset()

    return 120, 11

end


-- Performed when player arrives at object after object is clicked on
function winepool:onTouch()

    launchText( {{

        "", "You taste the viscous liquid. It tastes of wine."
    },
    {
        "", "Pity you have nothing to carry the wine with.",
    }})

end


-- Add candle to the bunch
return winepool
