-- chikun :: 2015
-- Object table


-- Table which holds object
local doorRight = {
    text    = "Marble Room",
    cursor  = "pointRight"
}


-- Performed when player needs to know where to go
function doorRight:getOffset()

    return  -8, 82

end


-- Performed when player arrives at object after object is clicked on
function doorRight:onTouch()

    -- Change to next map, teleport player to doorLeft
    map.change(maps.room1, "doorLeft")

end


-- Add object to the bunch
return doorRight
