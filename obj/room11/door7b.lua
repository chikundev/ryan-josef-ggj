-- chikun :: 2015
-- Object table


-- Table which holds object
local door7b = {
    text    = "Back to Safety",
    cursor  = "interact"
}


-- Performed when player needs to know where to go
function door7b:getOffset()

    return  16, 41

end


-- Performed when player arrives at object after object is clicked on
function door7b:onTouch()

   -- Change to next map and teleport player to 'doorLeft'
    map.change(maps.room8, "door3Forward")

end


-- Add object to the bunch
return door7b
