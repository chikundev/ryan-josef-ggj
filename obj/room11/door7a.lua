-- chikun :: 2015
-- Object table


-- Table which holds object
local door3Back = {
    text    = "Enter Network",
    cursor  = "interact"
}


-- Performed when player needs to know where to go
function door3Back:getOffset()

    return  16, 41

end


-- Performed when player arrives at object after object is clicked on
function door3Back:onTouch()

   -- Change to next map and teleport player to 'doorLeft'
    map.change(maps.room12, "gate")

end


-- Add object to the bunch
return door3Back
