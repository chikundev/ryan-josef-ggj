-- chikun :: 2015
-- Clock object in test room


-- Table which holds table object
local time = {
    text    = "World Time",
    cursor  = "interact",
    examine = {
        { "", "It's a display of the time across the world." }
    }
}


-- Performed when player needs to know where to go
function time:getOffset()

    return 112, 116

end


-- Performed when player arrives at object after object is clicked on
function time:onTouch()

    triggerDialogue("time")

end


-- Add clock to the bunch
return time
