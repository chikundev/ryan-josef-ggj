-- chikun :: 2015
-- Clock object in test room


-- Table which holds table object
local clippy = {
    text    = "Clippy",
    cursor  = "interact",
    examine = {
        { "", "It's Clippy. For Desktop PC computers." }
    }
}


-- Performed when player needs to know where to go
function clippy:getOffset()

    return 34, 100

end


-- Performed when player arrives at object after object is clicked on
function clippy:onTouch()

    triggerDialogue("clippy")

end


-- Add clock to the bunch
return clippy
