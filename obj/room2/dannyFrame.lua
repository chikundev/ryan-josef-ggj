-- chikun :: 2015
-- Table object in test room


-- Table which holds table object
local dannyFrame = {
    text    = "Portrait",
    cursor  = "interact",
    examine = {
        { "", " A portrait of musician-cum-composer Danny Elfman." },
        { "", "The portrait's contours ebb and flow before your eyes." },
        { "", "The portrait is alive." },
    }
}


-- Performed when player needs to know where to go
function dannyFrame:getOffset()

    return 30, 108

end


-- Performed when player arrives at object after object is clicked on
function dannyFrame:onTouch()

    if dannyMet2 then

        launchText({
                { "", "Danny Elfman has disappeared from this painting." }
            })

    elseif not dannyMet1 and not item.check('unholyLiquid') then

        triggerDialogue("danny")

        dannyMet1 = true

    elseif dannyMet1 and not item.check('unholyLiquid') then

        triggerDialogue("dannyMet")


    elseif not dannyMet2 and item.check('unholyLiquid') then

        dannyMet2 = true

        triggerDialogue("dannyAfterBowl")

    end



end


-- Add table to the bunch
return dannyFrame
