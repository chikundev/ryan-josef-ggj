-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local candle = {
    text    = "Candles",
    cursor  = "interact",
    examine = {
        {"", "These candles illuminate a large portrait."}
    }
}


-- Performed when player needs to know where to go
function candle:getOffset()

    return 16, 44

end


-- Performed when player arrives at object after object is clicked on
function candle:onTouch()

    launchText( {
            { "", "You try to touch the candles, but they're too hot." }
        })


end


-- Add candle to the bunch
return candle
