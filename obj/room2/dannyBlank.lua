-- chikun :: 2015
-- Table object in test room


-- Table which holds table object
local dannyBlank = {
    text    = " ",
    cursor  = "interact",
}


-- Performed when player needs to know where to go
function dannyBlank:getOffset()

    return 30, 108

end


-- Performed when player arrives at object after object is clicked on
function dannyBlank:onTouch()

        dannyMet2 = true


end


-- Add table to the bunch
return dannyBlank
