-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local mask1 = {
    text    = "Mask of Abassi",
    cursor  = "interact",
    examine = {
        {"", "Abassi, the Supreme Creator."}
    }
}


-- Performed when player needs to know where to go
function mask1:getOffset()

    return 16, 90

end


-- Performed when player arrives at object after object is clicked on
function mask1:onTouch()

    if spiritHappy1 then

        launchText({
                { "", "The spirit looks pleased." }
            })

    elseif spiritsAngry then


        launchText({
                { "", "..." }
            })

    else

        triggerDialogue("mask1")

    end

end


-- Add candle to the bunch
return mask1
