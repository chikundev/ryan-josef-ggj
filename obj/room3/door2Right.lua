-- chikun :: 2015
-- Object table


-- Table which holds object
local door2Right = {
    text    = "Superjoy Room",
    cursor  = "pointRight"
}


-- Performed when player needs to know where to go
function door2Right:getOffset()

    return  -8, 82

end


-- Performed when player arrives at object after object is clicked on
function door2Right:onTouch()

      -- Change to next map and teleport player to 'doorLeft'

    if spiritsAngry then
        state.load(states.facelessPanic)
    else
        map.change(maps.room4, "door2Left")
    end

end


-- Add object to the bunch
return door2Right
