-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local mask2 = {
    text    = "Mask of Yemaya",
    cursor  = "interact",
    examine = {
        {"", "A mask of Yemaya, Mother Goddess of the Ocean."}
    }
}


-- Performed when player needs to know where to go
function mask2:getOffset()

    return 16, 90

end


-- Performed when player arrives at object after object is clicked on
function mask2:onTouch()

    if spiritHappy2 then

        launchText({
                { "", "The spirit looks pleased." }
            })

    elseif spiritsAngry then


        launchText({
                { "", "..." }
            })

    else

        triggerDialogue("mask2")

    end

end


-- Add candle to the bunch
return mask2
