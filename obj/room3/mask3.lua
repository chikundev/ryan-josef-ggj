-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local mask3 = {
    text    = "Mask of Olorun",
    cursor  = "interact",
    examine = {
        {"", "A mask of Olorun, wise lord of the Heavens."}
    }
}


-- Performed when player needs to know where to go
function mask3:getOffset()

    return 16, 90

end


-- Performed when player arrives at object after object is clicked on
function mask3:onTouch()

    if spiritHappy3 then

        launchText({
                { "", "The spirit looks pleased." }
            })

    elseif spiritsAngry then


        launchText({
                { "", "..." }
            })

    else

        triggerDialogue("mask3")

    end

end


-- Add candle to the bunch
return mask3
