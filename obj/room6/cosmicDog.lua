-- chikun :: 2015
-- Clock object in test room


-- Table which holds table object
local cosmicDog = {
    text    = "Cosmic Dog",
    cursor  = "interact",
    examine = {
        { "", "It's Cosmic Dog!" }
    }
}


-- Performed when player needs to know where to go
function cosmicDog:getOffset()

    return 24, 80

end


-- Performed when player arrives at object after object is clicked on
function cosmicDog:onTouch()

    if not cosmicDogFood and item.check('dogFood') then

        item.setUsed('dogFood')

        triggerDialogue("cosmicDogFood")

    else

        triggerDialogue("cosmicDog")

    end

end


-- Add clock to the bunch
return cosmicDog
