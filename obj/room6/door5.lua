-- chikun :: 2015
-- Object table


-- Table which holds object
local door5 = {
    text    = "Medicine Room",
    cursor  = "pointRight"
}


-- Performed when player needs to know where to go
function door5:getOffset()

    return  -8, 82

end


-- Performed when player arrives at object after object is clicked on
function door5:onTouch()

      -- Change to next map and teleport player to 'doorLeft'

        map.change(maps.room7, "door4Left")

end


-- Add object to the bunch
return door5
