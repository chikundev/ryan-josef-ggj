-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local shiva = {
    text    = "Shiva",
    cursor  = "interact",
    examine = {
        {"", "This is Shiva, creator and destroyer of worlds."}
    }
}

-- Performed when player needs to know where to go
function shiva:getOffset()

    return 151, 102

end


-- Performed when player arrives at object after object is clicked on
function shiva:onTouch()

        triggerDialogue("shiva")

end


-- Add candle to the bunch
return shiva
