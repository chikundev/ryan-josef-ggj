-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local teakCabinet = {
    text    = "Teak Cabinet",
    cursor  = "interact",
    examine = {
        {"", "This is a small teak cabinet."}
    }
}


-- Performed when player needs to know where to go
function teakCabinet:getOffset()

    return 36, 61

end


-- Performed when player arrives at object after object is clicked on
function teakCabinet:onTouch()

    if peewee then

        launchText({
                { "", "After encountering Pee-Wee, you decide to leave the cabinet alone." }
            })

    else

        triggerDialogue("teakCabinet")

    end

end


-- Add candle to the bunch
return teakCabinet
