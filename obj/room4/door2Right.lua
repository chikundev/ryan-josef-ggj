-- chikun :: 2015
-- Object table


-- Table which holds object
local door2Right = {
    text    = "Fire Room",
    cursor  = "pointRight"
}


-- Performed when player needs to know where to go
function door2Right:getOffset()

    return  -8, 82

end


-- Performed when player arrives at object after object is clicked on
function door2Right:onTouch()

      -- Change to next map and teleport player to 'doorLeft'
    map.change(maps.room5, "door2Left")

end


-- Add object to the bunch
return door2Right
