-- chikun :: 2015
-- Chameleon object in jungle room


-- Table which holds chameleon object
local turtle = {
    text    = "Turtle",
    cursor  = "interact",
    examine = {
        {"", "A stuffed turtle."}
    }
}


-- Performed when player needs to know where to go
function turtle:getOffset()

    return 16, 47

end


-- Performed when player arrives at object after object is clicked on
function turtle:onTouch()

        triggerDialogue("turtle")

end

-- Add candle to the bunch
return turtle
