-- chikun :: 2015
-- Object table


-- Table which holds object
local door2Back = {
    text    = "Marble Room",
    cursor  = "interact"
}


-- Performed when player needs to know where to go
function door2Back:getOffset()

    return  16, 80

end


-- Performed when player arrives at object after object is clicked on
function door2Back:onTouch()

   -- Change to next map and teleport player to 'doorLeft'
    map.change(maps.room1, "doorForward")

end


-- Add object to the bunch
return door2Back
