-- chikun :: 2015
-- Forest Totem object in jungle room


-- Table which holds chameleon object
local forestTotem = {
    text    = "Forest Totem",
    cursor  = "interact",
    examine = {
        {"", "A weathered forest totem with an elongated tongue."}
    }
}


-- Performed when player needs to know where to go
function forestTotem:getOffset()

    return 32, 100

end


-- Performed when player arrives at object after object is clicked on
function forestTotem:onTouch()

        triggerDialogue("forestTotem")

end

-- Add candle to the bunch
return forestTotem
