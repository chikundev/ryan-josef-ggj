-- chikun :: 2015
-- Object table


-- Table which holds object
local doorLeft = {
    text    = "Offering Room",
    cursor  = "pointLeft"
}


-- Performed when player needs to know where to go
function doorLeft:getOffset()

    return  40, 82

end


-- Performed when player arrives at object after object is clicked on
function doorLeft:onTouch()

    -- Change to next map and teleport player to 'doorRight'
    map.change(maps.room9, "door7")

end


-- Add object to the bunch
return doorLeft
