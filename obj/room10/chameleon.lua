-- chikun :: 2015
-- Chameleon object in jungle room


-- Table which holds chameleon object
local chameleon = {
    text    = "Incapable Chameleon",
    cursor  = "interact",
    examine = {
        {"", "A chameleon with absolutely no clue."}
    }
}


-- Performed when player needs to know where to go
function chameleon:getOffset()

    return 57, 120

end


-- Performed when player arrives at object after object is clicked on
function chameleon:onTouch()

    if chameleon1 then

        launchText ({{ "", "The chameleon stares despondently." }})

    else

    triggerDialogue("chameleon")

    end

end

-- Add candle to the bunch
return chameleon
