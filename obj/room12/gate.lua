-- chikun :: 2015
-- Object table


-- Table which holds object
local gate = {
    text    = "The Singularity",
    cursor  = "interact"
}


-- Performed when player needs to know where to go
function gate:getOffset()

    return 50, 110

end


-- Performed when player arrives at object after object is clicked on
function gate:onTouch()

    if item.check('key1') and item.check('key2') then

        -- Change to next map and teleport player to 'doorLeft'
        map.change(maps.room13, "entrance")

    else

        launchText({
                { "", "Two psychic keyholes emerge from the cosmic portal." },
                { "", "Probably for keys that you don't have." }
            })

    end

end


-- Add object to the bunch
return gate
