-- chikun :: 2015
-- Object table


-- Table which holds object
local door2Left = {
    text    = "Superjoy Room",
    cursor  = "pointLeft"
}


-- Performed when player needs to know where to go
function door2Left:getOffset()

    return  40, 82

end


-- Performed when player arrives at object after object is clicked on
function door2Left:onTouch()

    -- Change to next map and teleport player to 'doorRight'
    map.change(maps.room4, "door2Right")

end


-- Add object to the bunch
return door2Left
