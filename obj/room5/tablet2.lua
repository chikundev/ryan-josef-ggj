-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local tablet2 = {
    text    = "Inscription",
    cursor  = "interact",
    examine = {
        {"", "An engraved inscription."}
    }
}


-- Performed when player needs to know where to go
function tablet2:getOffset()

    return 23, 118

end


-- Performed when player arrives at object after object is clicked on
function tablet2:onTouch()

        triggerDialogue("tablet2")

end


-- Add candle to the bunch
return tablet2
