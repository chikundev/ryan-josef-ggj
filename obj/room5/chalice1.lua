-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local chalice1 = {
    text    = "Chalice",
    cursor  = "interact",
    examine = {
        {"", "Golden chalices."}
    }
}


-- Performed when player needs to know where to go
function chalice1:getOffset()

    return 16, 40

end


-- Performed when player arrives at object after object is clicked on
function chalice1:onTouch()

    local desc = "The chalices, though beautiful in craft, are empty."

    if checkChalice(1) then

        desc = "The chalice is filled with red wine."

    end

    launchText( {
            { "", desc }
        })

end


-- Add candle to the bunch
return chalice1
