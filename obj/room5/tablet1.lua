-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local tablet1 = {
    text    = "Inscription",
    cursor  = "interact",
    examine = {
        {"", "An engraved inscription."}
    }
}


-- Performed when player needs to know where to go
function tablet1:getOffset()

    return 23, 118

end


-- Performed when player arrives at object after object is clicked on
function tablet1:onTouch()

        triggerDialogue("tablet1")
end


-- Add candle to the bunch
return tablet1
