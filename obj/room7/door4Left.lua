-- chikun :: 2015
-- Object table


-- Table which holds object
local door4Left = {
    text    = "Infinite Room",
    cursor  = "pointLeft"
}


-- Performed when player needs to know where to go
function door4Left:getOffset()

    return  40, 82

end


-- Performed when player arrives at object after object is clicked on
function door4Left:onTouch()

    -- Change to next map and teleport player to 'doorRight'
    map.change(maps.room6, "door5")

end


-- Add object to the bunch
return door4Left
