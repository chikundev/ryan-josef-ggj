-- chikun :: 2015
-- Table object in test room


-- Table which holds candle1 object
local medicineBuddha = {
    text    = "Medicine Buddha",
    cursor  = "interact",
    examine = {
        {"", "This is Bhaisajyaguru, the Medicine Buddha."}
    }
}


-- Performed when player needs to know where to go
function medicineBuddha:getOffset()

    return 34, 99

end


-- Performed when player arrives at object after object is clicked on
function medicineBuddha:onTouch()

    if medicineBuddha1 then

        launchText({
                { "", "The Medicine Buddha meditates in silence." }
            })

    else

        triggerDialogue("medicineBuddha")

    end

end


-- Add candle to the bunch
return medicineBuddha
