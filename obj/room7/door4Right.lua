-- chikun :: 2015
-- Object table


-- Table which holds object
local door4Right = {
    text    = "Yirrayanlong Room",
    cursor  = "pointRight"
}


-- Performed when player needs to know where to go
function door4Right:getOffset()

    return  -8, 82

end


-- Performed when player arrives at object after object is clicked on
function door4Right:onTouch()

      -- Change to next map and teleport player to 'doorLeft'

    if spiritsAngry then
        state.load(states.facelessPanic)
    else
        map.change(maps.room8, "door3Left")
    end

end


-- Add object to the bunch
return door4Right
