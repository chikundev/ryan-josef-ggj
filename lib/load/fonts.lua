-- chikun :: 2014
-- Loads all fonts from the /fnt folder


-- Can't do this recursively due to sizes
fnt = {
    -- Small font
    small   = love.graphics.newFont("fnt/onesize.ttf", 12),
    smallre = love.graphics.newFont("fnt/onesr.ttf", 12),
    -- Regular font
    regular = love.graphics.newFont("fnt/onesize.ttf", 24),
    -- Splash screen font
    splash  = love.graphics.newFont("fnt/exo2.otf", 36),
}

fnt.small:setLineHeight(16 / 14)
fnt.smallre:setLineHeight(16 / 14)
